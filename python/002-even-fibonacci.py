#!/usr/bin/env python

cache = {}

def fib(n):
    if n == 1:
        return 1
    elif n == 2:
        return 2
    else:
        if n not in cache:
            cache[n] = fib(n-1) + fib(n-2)
        return cache[n]


def main():
    i = 1
    f = fib(i)
    s = []
    while f < 4000000:
        i += 1
        f = fib(i)
        if f % 2 == 0:
            s.append(f)
    print(sum(s))


if __name__ == "__main__":
    main()
