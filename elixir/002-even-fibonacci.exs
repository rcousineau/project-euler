defmodule Euler do
  def calc_and_cache(n, cache) do
    # IO.puts("calculating fib for #{n}")
    calculated_value = fib(n-1, cache)[n-1] + fib(n-2, cache)[n-2]
    Map.put(cache, n, calculated_value)
  end

  def get_cached(n, cache) do
    case Map.fetch(cache, n) do
      {:ok, _} -> cache
      _ -> calc_and_cache(n, cache)
    end
  end

  def fib(n, cache) do
    case n do
      1 -> Map.put(cache, 1, 1)
      2 -> Map.put(cache, 2, 2)
      x -> get_cached(x, cache)
    end
  end
end

IO.puts("calculating...")
result = Stream.unfold(1, fn x -> {x, x + 1} end) |> Stream.filter(fn x -> Euler.fib(x, %{})[x] < 4_000_000 end) |> Enum.sum
IO.puts(result)
