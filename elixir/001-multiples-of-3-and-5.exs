result = 1..999 |> Stream.filter(fn x -> rem(x, 3) == 0 || rem(x, 5) == 0 end) |> Enum.sum
IO.puts(result)
