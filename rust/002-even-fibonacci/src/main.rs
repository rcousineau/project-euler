use std::collections::HashMap;

#[derive(Default)]
struct Fib {
    cache: HashMap<u32, u32>,
}

impl Fib {
    #[allow(clippy::map_entry)]
    fn fib(&mut self, n: u32) -> u32 {
        if n == 1 {
            1
        } else if n == 2 {
            2
        } else {
            if !self.cache.contains_key(&n) {
                println!("calculating fib({})", n);
                let a = self.fib(n - 1);
                let b = self.fib(n - 2);
                self.cache.insert(n, a + b);
            }
            *self.cache.get(&n).unwrap()
        }
    }
}

fn main() {
    let mut f = Fib::default();
    let result: u32 = (1..)
        .map(|n| f.fib(n))
        .take_while(|n| n < &4_000_000)
        .filter(|n| n % 2 == 0)
        .sum();
    println!("{}", result);
}
